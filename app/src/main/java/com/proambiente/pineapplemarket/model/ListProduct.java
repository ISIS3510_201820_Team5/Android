package com.proambiente.pineapplemarket.model;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import com.google.firebase.database.Exclude;

import java.util.ArrayList;
import java.util.Date;

@Entity
public class ListProduct {
    private int quantity;
    @NonNull
    @PrimaryKey
    private String id;
    private String image;
    private String name;
    @Ignore
    private ArrayList<String> tags;
    private String idList;
    private Date lastUpdate;
    private double bestPrice;
    private String bestMarketName;

    public ListProduct() {
    }

    public ListProduct(Product product) {
        this.setId(product.getId());
        this.setImage(product.getImage());
        this.setName(product.getName());
        this.setTags(product.getTags());
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Exclude
    public String getIdList() {
        return idList;
    }

    public void setIdList(String idList) {
        this.idList = idList;
    }

    @Exclude
    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Exclude
    public String getBestMarketName() {return bestMarketName;  }

    public void setBestMarketName(String bestMarketName) {this.bestMarketName = bestMarketName; }

    @Exclude
    public double getBestPrice() { return bestPrice; }

    public void setBestPrice(double bestPrice) {this.bestPrice = bestPrice; }
}
