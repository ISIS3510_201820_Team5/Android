package com.proambiente.pineapplemarket.model;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import android.net.Uri;
import androidx.annotation.NonNull;

@Entity
public class User {
    @PrimaryKey
    @NonNull
    private String id;
    private String name;
    private String email;
    @Ignore
    private Uri urlProfilePic;

    public User() {
    }

    public User(String id, String name, String email, Uri urlProfilePic) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.urlProfilePic = urlProfilePic;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Uri getUrlProfilePic() {
        return urlProfilePic;
    }

    public void setUrlProfilePic(Uri urlProfilePic) {
        this.urlProfilePic = urlProfilePic;
    }

}
