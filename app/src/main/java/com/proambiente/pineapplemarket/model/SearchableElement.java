package com.proambiente.pineapplemarket.model;

import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class SearchableElement implements Serializable {
    @PrimaryKey
    @NonNull
    private String id;
    private String name;
    private Date lastUpdate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Exclude
    public Date getLastUpdate() {
        if (lastUpdate == null)
            lastUpdate = Calendar.getInstance().getTime();
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
