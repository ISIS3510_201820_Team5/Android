package com.proambiente.pineapplemarket.model;

import android.util.Pair;

import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;

public class Factory {
    private Factory() {
    }

    public static ListProduct buildListProduct(DataSnapshot ds) {
        ListProduct pr = ds.getValue(ListProduct.class);
        pr.setId(ds.getKey());
        return pr;
    }

    public static Product buildProduct(DataSnapshot ds) {
        Product pr = ds.getValue(Product.class);
        pr.setId(ds.getKey());
        return pr;
    }

    public static java.util.List<Pair<String, Double>> buildPrices(DataSnapshot ds) {
        java.util.List<Pair<String, Double>> prices = new ArrayList<>();
        for (DataSnapshot data : ds.getChildren()) {
            prices.add(new Pair<>(data.getKey(), Double.parseDouble(data.getValue().toString())));
        }

        return prices;
    }

    public static java.util.List<Pair<String, Double>> buildHistoricalPrices(DataSnapshot ds) {
        java.util.List<Pair<String, Double>> prices = new ArrayList<>();
        prices.add(new Pair<>(ds.getKey(), Double.parseDouble(ds.child("average").getValue().toString())));
        return prices;
    }

    public static List buildList(DataSnapshot ds) {
        List ls = ds.getValue(List.class);
        ls.setId(ds.getKey());
        return ls;
    }

    public static Market buildMarket(DataSnapshot ds) {
        Market ls = ds.getValue(Market.class);
        return ls;
    }

    public static User buildUser(DataSnapshot ds) {
        User ls = ds.getValue(User.class);
        return ls;
    }
}
