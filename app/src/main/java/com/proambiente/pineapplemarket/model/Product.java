package com.proambiente.pineapplemarket.model;

import androidx.room.Entity;
import androidx.room.Ignore;
import android.util.Pair;

import java.sql.Timestamp;
import java.util.ArrayList;

@Entity
public class Product extends SearchableElement {
    @Ignore
    private ArrayList<String> tags;
    private String image;
    @Ignore
    private ArrayList<Pair<Timestamp, Double>> historicalPrices;
    @Ignore
    private ArrayList<Pair<String, Double>> prices;

    public ArrayList<Pair<String, Double>> getPrices() {
        return prices;
    }

    public void setPrices(ArrayList<Pair<String, Double>> prices) {
        this.prices = prices;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public ArrayList<Pair<Timestamp, Double>> getHistoricalPrices() {
        return historicalPrices;
    }

    public void setHistoricalPrices(ArrayList<Pair<Timestamp, Double>> historicalPrices) {
        this.historicalPrices = historicalPrices;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "{name:" + getName() + ", barcode:" + getId() + "}";
    }
}
