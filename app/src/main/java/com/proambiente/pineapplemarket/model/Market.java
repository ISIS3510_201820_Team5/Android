package com.proambiente.pineapplemarket.model;

import androidx.room.Entity;
import android.graphics.Bitmap;
import android.util.Pair;

import java.util.ArrayList;

@Entity
public class Market extends SearchableElement {
    private ArrayList<Pair<Double, Double>> locations;
    private Bitmap logo;

    public ArrayList<Pair<Double, Double>> getLocations() {
        return locations;
    }

    public void setLocations(ArrayList<Pair<Double, Double>> locations) {
        this.locations = locations;
    }

    public Bitmap getLogo() {
        return logo;
    }

    public void setLogo(Bitmap logo) {
        this.logo = logo;
    }
}