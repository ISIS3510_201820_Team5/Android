package com.proambiente.pineapplemarket.model;

import androidx.room.Entity;
import androidx.room.Ignore;

import java.util.ArrayList;
import java.util.Set;

@Entity
public class List extends SearchableElement {
    private boolean owner;
    private ArrayList<Product> products;
    @Ignore
    private Set<ListUser> participants;
    private int order;
    private String userId;

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public boolean isOwner() {
        return owner;
    }

    public void setOwner(boolean owner) {
        this.owner = owner;
    }

    public Set<ListUser> getParticipants() {
        return participants;
    }

    public void setParticipants(Set<ListUser> participants) {
        this.participants = participants;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "{List: " + getName() + ", id:" + getId() + ", owner:" + owner + "}";
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
