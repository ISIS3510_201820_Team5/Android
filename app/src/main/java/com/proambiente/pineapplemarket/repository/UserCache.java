package com.proambiente.pineapplemarket.repository;

import androidx.lifecycle.MutableLiveData;
import android.util.Pair;

import com.proambiente.pineapplemarket.model.Product;

import java.util.HashMap;
import java.util.List;

public class UserCache {
    private MutableLiveData<List<Product>> lastProducts;
    //private MutableLiveData<List<Product>> searchProducts;
    private MutableLiveData<List<com.proambiente.pineapplemarket.model.List>> lastLists;
    private HashMap<String, MutableLiveData<List<List<Pair<String, Double>>>>> prices;

    UserCache() {
        lastProducts = new MutableLiveData<>();
        lastLists = new MutableLiveData<>();
        prices = new HashMap<>();
    }

    public MutableLiveData<List<com.proambiente.pineapplemarket.model.List>> getLastLists(String userId) {
        return lastLists;
    }

    public void setLastLists(MutableLiveData<List<com.proambiente.pineapplemarket.model.List>> lastLists) {
        this.lastLists = lastLists;
    }

    public HashMap<String, MutableLiveData<List<List<Pair<String, Double>>>>> getPrices() {
        return prices;
    }

    public MutableLiveData<List<Product>> getLastProducts() {
        return lastProducts;
    }

    public void setLastProducts(MutableLiveData<List<Product>> lastProducts) {
        this.lastProducts = lastProducts;
    }

    public MutableLiveData<List<List<Pair<String, Double>>>> getPrices(String productId) {
        return this.prices.get(productId);
    }

    public void putPrices(String productId,
                          MutableLiveData<List<List<Pair<String, Double>>>> prices) {
        this.prices.put(productId, prices);
    }

    /*
    public MutableLiveData<List<Product>> getSearchProducts() {
        return searchProducts;
    }

    public void setSearchProducts(MutableLiveData<List<Product>> searchProducts) {
        this.searchProducts = searchProducts;
    }*/
}
