package com.proambiente.pineapplemarket.repository;

import android.app.Application;
import android.util.Pair;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.proambiente.pineapplemarket.model.List;
import com.proambiente.pineapplemarket.model.ListProduct;
import com.proambiente.pineapplemarket.model.Product;
import com.proambiente.pineapplemarket.utils.Converters;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

public class DataRepository {
    private static final int TIMEOUT_TO_REFRESH = 10; //minutes
    private static final int KEEP_ALIVE = 1;
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;
    private static final int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
    private static volatile DataRepository repository;
    private final Executor executor;
    private DataManager source;
    private RoomDao persistence;
    private UserDatabase userDatabase;

    private DataRepository(Application application) {
        source = FirebaseManager.getInstance(application);
        userDatabase = UserDatabase.getInstance(application.getApplicationContext());
        persistence = userDatabase.roomDao();
        BlockingQueue<Runnable> queueTasks = new LinkedBlockingQueue<>();

        executor = new ThreadPoolExecutor(
                NUMBER_OF_CORES, NUMBER_OF_CORES, KEEP_ALIVE,
                KEEP_ALIVE_TIME_UNIT, queueTasks);
    }

    public static DataRepository getInstance(Application application) {
        if (repository == null) {
            synchronized (DataRepository.class) {
                if (repository == null) {
                    repository = new DataRepository(application);
                }
            }
        }
        return repository;
    }

    private void saveLists(java.util.List<List> lists) {
        if (lists == null) return;
        persistence.deleteLists();
        persistence.saveLists(lists);
    }

    public LiveData<java.util.List<List>> getAllLists() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MINUTE, TIMEOUT_TO_REFRESH);
        executor.execute(() -> {
            long time = Converters.dateToTimestamp(c.getTime());
            int needUpdate = persistence.listsNeedUpdate(time);
            if (persistence.listsSize() == 0 || needUpdate > 0) {
                saveLists(source.getAllLists().getValue());
            }
        });

        source.getAllLists().observeForever(lists -> executor.execute(() ->
                saveLists(lists)
        ));

        return persistence.getAllLists();
    }

    public void addList(String name) {
        List newList = new List();
        newList.setName(name);
        newList.setOwner(true);
        newList.setId(UUID.randomUUID().toString());

        source.addList(newList);
        executor.execute(() -> persistence.saveList(newList));
    }

    private void saveListProducts(java.util.List<ListProduct> products) {
        if (products == null) return;
        persistence.deleteProducts();
        persistence.saveListProducts(products);
    }

    private void saveProducts(java.util.List<Product> products) {
        if (products == null) return;
        persistence.deleteProducts();
        persistence.saveProducts(products);
    }

    public LiveData<java.util.List<Product>> loadAllProducts(int limit) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MINUTE, TIMEOUT_TO_REFRESH);

        executor.execute(() -> {
            long time = Converters.dateToTimestamp(c.getTime());
            if (persistence.listsSize() == 0 || persistence.productsNeedUpdate(time) > 0) {
                saveProducts(source.loadAllProducts(limit).getValue());
            }
        });

        source.loadAllProducts(limit).observeForever(products -> executor.execute(() ->
                saveProducts(products)
        ));

        return persistence.getProducts(limit);
    }

    public MutableLiveData<java.util.List<java.util.List<Pair<String, Double>>>> getPricesByProduct(String idProduct) {
        /*Calendar c = Calendar.getInstance();
        c.add(Calendar.MINUTE, TIMEOUT_TO_REFRESH);

        executor.execute(() -> {
            long time = Converters.dateToTimestamp(c.getTime());
            if (persistence.listsSize() == 0 || persistence.productsNeedUpdate(time) > 0) {
                source.getPricesByProduct(idProduct).observeForever(products ->
                        executor.execute(() -> {
                            persistence.products
                        }));
            }
        });*/

        return source.getPricesByProduct(idProduct);
    }

    public MutableLiveData<java.util.List<java.util.List<Pair<String, Double>>>> getHistoricalPricesByProduct(String idProduct) {
        return source.getHistoricalPricesByProduct(idProduct);
    }

    public LiveData<java.util.List<ListProduct>> getProductsInList(String idList) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MINUTE, TIMEOUT_TO_REFRESH);

        executor.execute(() -> {
            long time = Converters.dateToTimestamp(c.getTime());
            if (persistence.productsSize(idList) == 0 || persistence.listProductsNeedUpdate(time, idList) > 0) {
                saveListProducts(source.getProductsInList(idList).getValue());
            }
        });

        source.getProductsInList(idList).observeForever(products ->
                executor.execute(() -> saveListProducts(products)));

        return persistence.getProductsInList(idList);
    }

    public void addStateListener(FirebaseAuth.AuthStateListener listener) {
        source.addStateListener(listener);
    }

    public void removeStateListener(FirebaseAuth.AuthStateListener listener) {
        source.removeStateListener(listener);
    }

    public LiveData<Boolean> addProductToList(Product product, String listId, int quantity) {
        ListProduct listProduct = new ListProduct(product);
        listProduct.setIdList(listId);
        listProduct.setQuantity(quantity);

        MutableLiveData<Boolean> result = new MutableLiveData<>();
        LiveData<java.util.List<ListProduct>> lp = persistence.productInList(listId, listProduct.getId());

        Observer<java.util.List<ListProduct>> observer = listProducts -> {
            if (listProducts != null && listProducts.size() > 0) {
                result.postValue(false);
            } else {
                source.addProductToList(listProduct).addOnCompleteListener(task1 ->
                        executor.execute(() -> {
                            persistence.addProductToList(listProduct);
                            result.postValue(true);
                        }));
            }
        };

        lp.observeForever(observer);

        result.observeForever(aBoolean -> {
            if (lp.hasActiveObservers())
                lp.removeObserver(observer);
        });
        return result;
    }

    public MutableLiveData<java.util.List<Product>> getElementsStartWith(String s) {
        return source.getElementsStartWith(s);
    }

    public void deleteProductInList(String listId, String product) {
        source.deleteProductInList(listId, product);
        executor.execute(() -> persistence.deleteProductInList(listId, product));
    }

    public FirebaseUser getCurrentUser() {
        return source.getCurrentUser();
    }

    public Task<AuthResult> performRegister(String email, String password) {
        return source.performRegister(email, password);
    }

    public Task<AuthResult> performSignIn(String email, String password) {
        return source.performSignIn(email, password);
    }

    public void addNewUser(FirebaseUser currentUser) {
        source.addNewUser(currentUser);
    }

    public void updateProductQuantityInList(String listId, String productId, int quantity) {
        source.updateProductQuantityInList(listId, productId, quantity);
        executor.execute(() ->
                persistence.updateProductQuantityInList(listId, productId, quantity));
    }

    public void performSignOut() {
        source.performSignOut();
        executor.execute(() ->
                userDatabase.clearAllTables());
    }

    public Task<Void> setUserName(@NotNull String name) {
        return source.setUserName(name);
    }

    public Task<AuthResult> performSignInWithGoogle(GoogleSignInAccount account) {
        return source.performSignInWithGoogle(account);
    }

    public MutableLiveData<Product> getProduct(@NotNull String displayValue) {
        return source.getProduct(displayValue);
    }
}
