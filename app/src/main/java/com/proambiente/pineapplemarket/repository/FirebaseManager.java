package com.proambiente.pineapplemarket.repository;

import android.app.Application;
import android.content.res.Resources;
import android.net.Uri;
import android.util.Pair;
import android.widget.ImageView;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.proambiente.pineapplemarket.model.ListProduct;
import com.proambiente.pineapplemarket.model.ListUser;
import com.proambiente.pineapplemarket.model.Product;
import com.proambiente.pineapplemarket.utils.Factory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

class FirebaseManager extends DataManager {
    private static final String PRODUCTS_REF = "/products";
    private static final String LISTS_REF = "/lists";
    private static final String USERS_REF = "/users";
    private static final String PRICES_REF = "/prices";
    private static final String QUANTITY_REF = "/quantity";
    private static final String PRODUCT_IMAGE_NAME = "image.jpg";
    private static final String PRODUCT_THUMBNAIL_NAME = "thumbnail.jpg";

    private static FirebaseManager fm;
    private final FirebaseDatabase database;
    private final FirebaseAuth auth;
    private final FirebaseStorage storage;
    private FirebaseUser user;
    private Resources resources;
    private Application application;

    private FirebaseManager(Application application) {
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        database = FirebaseDatabase.getInstance();
        storage = FirebaseStorage.getInstance();
        resources = application.getResources();
        this.application = application;
    }

    public static FirebaseManager getInstance(Application application) {
        if (fm == null) {
            fm = new FirebaseManager(application);
        }
        return fm;
    }

    @Override
    public MutableLiveData<List<com.proambiente.pineapplemarket.model.List>> getAllLists() {
        MutableLiveData<List<com.proambiente.pineapplemarket.model.List>> lists;

        final FirebaseQueryLiveData liveData = new FirebaseQueryLiveData(
                database.getReference(USERS_REF).child(user.getUid() + LISTS_REF));

        lists = (MutableLiveData<List<com.proambiente.pineapplemarket.model.List>>)
                Transformations.map(liveData, new ListsDeserializer());
        return lists;
    }

    @Override
    public MutableLiveData<List<List<Pair<String, Double>>>> getPricesByProduct(String idProduct) {
        MutableLiveData<List<List<Pair<String, Double>>>> prices;

        final QueryLiveData liveData = new QueryLiveData(
                database.getReference(PRICES_REF).child(idProduct).orderByKey().limitToFirst(1));

        prices = (MutableLiveData<List<List<Pair<String, Double>>>>)
                Transformations.map(liveData, new PricesDeserializer());
        return prices;
    }

    @Override
    public Task<AuthResult> performRegister(String email, String password) {
        Task<AuthResult> task = auth.createUserWithEmailAndPassword(email, password);
        task.addOnCompleteListener(task1 -> {
            if (task1.isSuccessful() && task1.getResult() != null)
                user = task1.getResult().getUser();
        });
        return task;
    }

    @Override
    public Task<AuthResult> performSignIn(String email, String password) {
        return auth.signInWithEmailAndPassword(email, password);
    }

    @Override
    public Task<Void> setUserName(String name) {
        UserProfileChangeRequest request = new UserProfileChangeRequest.Builder()
                .setDisplayName(name)
                .build();
        return user.updateProfile(request);
    }

    @Override
    public MutableLiveData<List<List<Pair<String, Double>>>> getHistoricalPricesByProduct(String idProduct) {
        final QueryLiveData liveData = new QueryLiveData(
                database.getReference(PRICES_REF).child(idProduct).orderByKey().limitToFirst(8));

        MutableLiveData<List<List<Pair<String, Double>>>> prices = (MutableLiveData<List<List<Pair<String, Double>>>>)
                Transformations.map(liveData, new HistoricalPricesDeserializer());
        return prices;
    }

    @Override
    public MutableLiveData<List<Product>> getElementsStartWith(String sub) {
        MutableLiveData<List<Product>> searchProducts;

        QueryLiveData liveData = new QueryLiveData(database.getReference(PRODUCTS_REF)
                .orderByChild("name").startAt(sub).endAt(sub + "\uf8ff"));

        searchProducts = (MutableLiveData<List<Product>>)
                Transformations.map(liveData, new ProductsDeserializer());

        return searchProducts;
    }

    @Override
    public MutableLiveData<List<Product>> loadAllProducts(int limit) {
        MutableLiveData<List<Product>> products;

        QueryLiveData liveData = new QueryLiveData(database.getReference(PRODUCTS_REF).limitToFirst(limit));

        products = (MutableLiveData<List<Product>>)
                Transformations.map(liveData, new ProductsDeserializer());
        return products;
    }

    @Override
    public MutableLiveData<Product> getProduct(String displayValue) {
        QueryLiveData query = new QueryLiveData(database.getReference(PRODUCTS_REF)
                .child(displayValue));
        return (MutableLiveData<Product>)
                Transformations.map(query, new ProductDeserializer());
    }

    @Override
    public LiveData<ListProduct> productExistsInList(String idList, String idProduct) {
        QueryLiveData query = new QueryLiveData(database.getReference(LISTS_REF)
                .child(idList + PRODUCTS_REF).child(idProduct));
        return Transformations.map(query, new ListProductDeserializer());
    }

    @Override
    public Task<Void> addProductToList(ListProduct listProduct) {
        DatabaseReference reference = database.getReference(LISTS_REF)
                .child(listProduct.getIdList() + PRODUCTS_REF).child(listProduct.getId());
        return reference.setValue(listProduct);
    }

    @Override
    public void addList(com.proambiente.pineapplemarket.model.List list) {
        database.getReference(USERS_REF).child(user.getUid() + LISTS_REF)
                .child(list.getId()).setValue(list);

        ListUser listUser = new ListUser();
        listUser.setUserId(user.getUid());
        listUser.setName(user.getDisplayName());

        database.getReference(LISTS_REF).child(list.getId()).setValue(listUser);
    }

    @Override
    public void addStateListener(FirebaseAuth.AuthStateListener listener) {
        auth.addAuthStateListener(listener);
    }

    @Override
    public void removeStateListener(FirebaseAuth.AuthStateListener listener) {
        auth.removeAuthStateListener(listener);
    }

    @Override
    public Task<AuthResult> performSignInWithGoogle(GoogleSignInAccount account) {
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getId(), null);
        Task<AuthResult> task = auth.signInWithCredential(credential);
        task.addOnCompleteListener(task1 -> {
            if (task1.isSuccessful() && task1.getResult() != null) {
                user = task1.getResult().getUser();
            }
        });
        return task;
    }

    @Override
    public void performSignOut() {
        auth.signOut();
    }

    @Override
    public void addNewUser(FirebaseUser user) {
        this.user = user;

        Map<String, Object> map = new HashMap<>();
        map.put("name", user.getDisplayName());
        map.put("email", user.getEmail());

        database.getReference(USERS_REF).child(user.getUid()).updateChildren(map);
    }

    @Override
    public MutableLiveData<List<ListProduct>> getProductsInList(String id) {
        QueryLiveData query = new QueryLiveData(database.getReference(LISTS_REF).child(id + PRODUCTS_REF));
        MutableLiveData<List<ListProduct>> products;
        products = (MutableLiveData<List<ListProduct>>)
                Transformations.map(query, new ListProductsDeserializer(id));
        return products;
    }

    @Override
    public FirebaseUser getCurrentUser() {
        return FirebaseAuth.getInstance().getCurrentUser();
    }

    @Override
    public void deleteProductInList(String listId, String productId) {
        database.getReference(LISTS_REF).child(listId + PRODUCTS_REF).child(productId).removeValue();
    }

    @Override
    public void updateProductQuantityInList(String listId, String productId, int quantity) {
        database.getReference(LISTS_REF).child(listId + PRODUCTS_REF)
                .child(productId + QUANTITY_REF).setValue(quantity);
    }

    @Override
    public void loadProductImageIntoView(ImageView imageView, String productId) {
        storage.getReference(PRODUCTS_REF).child(productId).getFile(Uri.parse(PRODUCT_IMAGE_NAME));

    }

    private class ListProductsDeserializer implements Function<DataSnapshot, List<ListProduct>> {
        private String idList;

        ListProductsDeserializer(String idList) {
            this.idList = idList;
        }

        @Override
        public ArrayList<ListProduct> apply(DataSnapshot input) {
            if (input == null)
                return null;

            Iterator<DataSnapshot> it = input.getChildren().iterator();

            ArrayList<ListProduct> products = new ArrayList<>();
            while (it.hasNext()) {
                DataSnapshot ds = it.next();
                ListProduct pr = Factory.buildListProduct(ds);
                pr.setIdList(idList);
                products.add(pr);
            }

            return products;
        }
    }

    private class ListProductDeserializer implements Function<DataSnapshot, ListProduct> {

        @Override
        public ListProduct apply(DataSnapshot input) {
            if (input == null || !input.exists()) return null;
            return Factory.buildListProduct(input);
        }
    }

    private class ProductDeserializer implements Function<DataSnapshot, Product> {
        @Override
        public Product apply(DataSnapshot input) {
            if (input == null)
                return null;
            return Factory.buildProduct(input);
        }
    }

    private class ProductsDeserializer implements Function<DataSnapshot, List<Product>> {
        @Override
        public ArrayList<Product> apply(DataSnapshot input) {
            if (input == null)
                return null;

            Iterator<DataSnapshot> it = input.getChildren().iterator();
            ArrayList<Product> products = new ArrayList<>();
            while (it.hasNext()) {
                DataSnapshot ds = it.next();
                Product pr = Factory.buildProduct(ds);
                products.add(pr);
            }
            return products;
        }
    }

    private class ListsDeserializer implements Function<DataSnapshot, List<com.proambiente.pineapplemarket.model.List>> {
        @Override
        public ArrayList<com.proambiente.pineapplemarket.model.List> apply(DataSnapshot input) {
            if (input == null)
                return null;
            ArrayList<com.proambiente.pineapplemarket.model.List> lists = new ArrayList<>();

            for (DataSnapshot ds : input.getChildren()) {
                lists.add(Factory.buildList(ds));
            }
            return lists;
        }
    }

    private class PricesDeserializer implements Function<DataSnapshot, List<List<Pair<String, Double>>>> {
        @Override
        public List<List<Pair<String, Double>>> apply(DataSnapshot input) {
            if (input == null)
                return null;
            List<List<Pair<String, Double>>> prices = new ArrayList<>();

            for (DataSnapshot ds : input.getChildren()) {
                prices.add(Factory.buildPrices(ds));
            }

            return prices;
        }
    }

    private class HistoricalPricesDeserializer implements Function<DataSnapshot, List<List<Pair<String, Double>>>> {
        @Override
        public List<List<Pair<String, Double>>> apply(DataSnapshot input) {
            if (input == null)
                return null;
            List<List<Pair<String, Double>>> prices = new ArrayList<>();

            for (DataSnapshot ds : input.getChildren()) {
                prices.add(Factory.buildHistoricalPrices(ds));
            }

            return prices;
        }
    }

    public class QueryLiveData extends LiveData<DataSnapshot> {
        private final Query query;
        private final ValueEventListener listener = new CustomListener();

        public QueryLiveData(Query query) {
            this.query = query;
        }

        @Override
        protected void onActive() {
            query.addValueEventListener(listener);
        }

        @Override
        protected void onInactive() {
            query.removeEventListener(listener);
        }

        private class CustomListener implements ValueEventListener {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                setValue(dataSnapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        }
    }
}