package com.proambiente.pineapplemarket.repository;

import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

class FirebaseQueryLiveData extends LiveData<DataSnapshot> {
    private final Query query;
    private final ValueEventListener listener = new CustomListener();

    public FirebaseQueryLiveData(DatabaseReference databaseReference) {
        this.query = databaseReference;
    }

    @Override
    protected void onActive() {
        query.addValueEventListener(listener);
    }

    @Override
    protected void onInactive() {
        query.removeEventListener(listener);
    }

    private class CustomListener implements ValueEventListener {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            setValue(dataSnapshot);
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
        }
    }
}
