package com.proambiente.pineapplemarket.repository;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import android.content.Context;

import com.proambiente.pineapplemarket.model.List;
import com.proambiente.pineapplemarket.model.ListProduct;
import com.proambiente.pineapplemarket.model.Product;
import com.proambiente.pineapplemarket.model.User;
import com.proambiente.pineapplemarket.utils.Converters;

@Database(entities = {User.class, Product.class, List.class, ListProduct.class}, version = 1)
@TypeConverters(Converters.class)
public abstract class UserDatabase extends RoomDatabase {
    private static volatile UserDatabase database;

    public static UserDatabase getInstance(final Context context) {
        if (database == null) {
            synchronized (UserDatabase.class) {
                if (database == null) {
                    database = Room.databaseBuilder(context.getApplicationContext(),
                            UserDatabase.class, "user_database").build();
                }
            }
        }
        return database;
    }

    public abstract RoomDao roomDao();
}
