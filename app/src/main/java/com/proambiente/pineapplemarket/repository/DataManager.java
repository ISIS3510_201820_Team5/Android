package com.proambiente.pineapplemarket.repository;

import android.util.Pair;
import android.widget.ImageView;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.proambiente.pineapplemarket.model.ListProduct;
import com.proambiente.pineapplemarket.model.Product;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

abstract class DataManager {
    public abstract MutableLiveData<java.util.List<Product>> loadAllProducts(int limit);

    public abstract MutableLiveData<List<Product>> getElementsStartWith(String sub);

    public abstract MutableLiveData<List<com.proambiente.pineapplemarket.model.List>> getAllLists();

    public abstract void addList(com.proambiente.pineapplemarket.model.List name);

    public abstract MutableLiveData<List<List<Pair<String, Double>>>> getPricesByProduct(String idProduct);

    public abstract MutableLiveData<List<List<Pair<String, Double>>>> getHistoricalPricesByProduct(String idProduct);

    public abstract Task<Void> addProductToList(ListProduct listProduct);

    public abstract LiveData<ListProduct> productExistsInList(String idList, String idProduct);

    public abstract void addStateListener(FirebaseAuth.AuthStateListener listener);

    public abstract void removeStateListener(FirebaseAuth.AuthStateListener listener);

    public abstract void performSignOut();

    public abstract void addNewUser(FirebaseUser user);

    public abstract FirebaseUser getCurrentUser();

    public abstract MutableLiveData<List<ListProduct>> getProductsInList(String id);

    public abstract void deleteProductInList(String listId, String productId);

    public abstract void updateProductQuantityInList(String listId, String productId, int quantity);

    public abstract void loadProductImageIntoView(ImageView imageView, String productId);

    public abstract Task<AuthResult> performSignIn(String email, String password);

    public abstract Task<AuthResult> performRegister(String email, String password);

    public abstract Task<Void> setUserName(String name);

    public abstract Task<AuthResult> performSignInWithGoogle(GoogleSignInAccount account);

    public abstract MutableLiveData<Product> getProduct(String displayValue);
}
