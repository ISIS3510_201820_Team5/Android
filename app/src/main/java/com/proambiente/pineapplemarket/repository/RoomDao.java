package com.proambiente.pineapplemarket.repository;

import com.proambiente.pineapplemarket.model.List;
import com.proambiente.pineapplemarket.model.ListProduct;
import com.proambiente.pineapplemarket.model.Product;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface RoomDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveProducts(java.util.List<Product> products);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveListProducts(java.util.List<ListProduct> listProducts);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveList(List list);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveLists(java.util.List<List> list);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addProductToList(ListProduct listProduct);

    @Query("DELETE FROM List")
    void deleteLists();

    @Query("SELECT * FROM List ORDER BY name")
    LiveData<java.util.List<List>> getAllLists();

    @Query("SELECT COUNT(*) FROM List")
    int listsSize();

    @Query("SELECT COUNT(*) FROM List WHERE lastUpdate < :timeout")
    int listsNeedUpdate(Long timeout);

    @Query("SELECT * FROM ListProduct WHERE idList == :idList AND id == :idProduct")
    LiveData<java.util.List<ListProduct>> productInList(String idList, String idProduct);

    @Query("SELECT * FROM Product LIMIT :limit")
    LiveData<java.util.List<Product>> getProducts(int limit);

    @Query("SELECT COUNT(*) FROM Product WHERE lastUpdate < :timeout")
    int productsNeedUpdate(Long timeout);

    @Query("DELETE FROM Product")
    void deleteProducts();

    @Query("SELECT * FROM ListProduct WHERE idList == :idList")
    LiveData<java.util.List<ListProduct>> getProductsInList(String idList);

    @Query("SELECT COUNT(*) FROM ListProduct WHERE lastUpdate < :timeout AND idList == :idList")
    int listProductsNeedUpdate(Long timeout, String idList);

    @Query("SELECT COUNT(*) FROM ListProduct WHERE idList == :idList")
    int productsSize(String idList);

    @Query("DELETE FROM ListProduct WHERE idList == :idList")
    void deleteListProducts(String idList);

    @Query("UPDATE ListProduct SET quantity = :quantity WHERE idList == :idList AND id == :idProduct")
    void updateProductQuantityInList(String idList, String idProduct, int quantity);

    @Query("DELETE FROM ListProduct WHERE idList == :idList AND id == :idProduct")
    void deleteProductInList(String idList, String idProduct);
}