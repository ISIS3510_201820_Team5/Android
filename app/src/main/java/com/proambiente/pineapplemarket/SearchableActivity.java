package com.proambiente.pineapplemarket;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;

import com.mancj.materialsearchbar.MaterialSearchBar;
import com.proambiente.pineapplemarket.model.Product;
import com.proambiente.pineapplemarket.model.SearchableElement;
import com.proambiente.pineapplemarket.repository.DataRepository;
import com.proambiente.pineapplemarket.ui.home.SearchableElementsAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.disposables.Disposable;

public class SearchableActivity extends AppCompatActivity {
    private final static int MAX_SEARCH_TEXT_LENGTH = 50;
    private Set<String> suggest = new HashSet<>();
    private List<SearchableElement> localDataSource = new ArrayList<>();
    private RecyclerView recyclerSearch;
    private SearchableElementsAdapter adapter;
    private MaterialSearchBar bar;
    private DataRepository repository;
    private Disposable helperSearch;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        bar.enableSearch();
        bar.requestFocus();
        bar.callOnClick();
        bar.showSuggestionsList();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity);
        repository = DataRepository.getInstance(getApplication());

        recyclerSearch = findViewById(R.id.search_suggestions);
        recyclerSearch.setLayoutManager(new GridLayoutManager(this, 2));

        adapter = new SearchableElementsAdapter(localDataSource);
        recyclerSearch.setAdapter(adapter);

        bar = findViewById(R.id.search_bar);
        bar.setCardViewElevation(10);
        bar.callOnClick();
        bar.setNavButtonEnabled(true);
        bar.setMenuIcon(R.drawable.ic_action_back);
        bar.setArrowIcon(R.drawable.ic_action_back);
        bar.refreshDrawableState();

        bar.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener() {
            @Override
            public void onSearchStateChanged(boolean enabled) {
            }

            @Override
            public void onSearchConfirmed(CharSequence text) {
            }

            @Override
            public void onButtonClicked(int buttonCode) {
                switch (buttonCode) {
                    case MaterialSearchBar.BUTTON_NAVIGATION:
                        SearchableActivity.this.onBackPressed();
                        break;
                    case MaterialSearchBar.BUTTON_BACK:
                        SearchableActivity.this.onBackPressed();
                        break;
                }
            }
        });

        repository.loadAllProducts(10).observe(this, products -> {
            assert products != null;
            displaySearchableElements(products);
        });

        ObservableOnSubscribe<CharSequence> source = emitter -> bar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                emitter.onNext(s);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() >= MAX_SEARCH_TEXT_LENGTH) {
                    CharSequence sq = s.subSequence(0, MAX_SEARCH_TEXT_LENGTH - 1);
                    s.clear();
                    s.insert(0, sq);
                }
            }
        });

        helperSearch = Observable.create(source)
                .map(text -> text.toString().replaceAll("(\\s)+", " "))
                .map(text -> text.replaceAll("^(\\s)+|(\\s)+$", ""))
                .debounce(300, TimeUnit.MILLISECONDS)
                //.distinct()
                .filter(text -> text.matches("[(\\s)*(\\w)|\\w]+"))
                .subscribe(o -> {
                    MutableLiveData<List<Product>> products = repository.getElementsStartWith(o);
                    runOnUiThread(() -> products.observe(this, products1 -> {
                        if (products1 != null) {
                            displaySearchableElements(products1);
                            buildLastSuggestions(products1);
                        }
                    }));
                });
    }

    private void buildLastSuggestions(@NonNull List elements) {
        suggest.clear();
        for (Object o : elements) {
            SearchableElement element = (SearchableElement) o;
            suggest.add(element.getName());
        }
        bar.clearSuggestions();
        bar.updateLastSuggestions(Arrays.asList(suggest.toArray()));
    }

    private void displaySearchableElements(List elements) {
        localDataSource = elements;
        adapter.setItems(localDataSource);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        helperSearch.dispose();
    }
}
