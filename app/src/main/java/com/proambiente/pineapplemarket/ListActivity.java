package com.proambiente.pineapplemarket;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Window;

import com.proambiente.pineapplemarket.ui.home.ProductsFragment;
import com.proambiente.pineapplemarket.ui.list.ProductsListFragment;
import com.proambiente.pineapplemarket.ui.list.UserListAdapterVertical;

public class ListActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        Bundle args = getIntent().getExtras();
        String id = args.getString(UserListAdapterVertical.ARG_LIST_ID);
        String name = args.getString(UserListAdapterVertical.ARG_LIST_NAME);

        getSupportActionBar().setTitle(name);

        Fragment productsFragment = ProductsListFragment.newInstance(id, name);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.recycler_list_products, productsFragment,
                        ProductsFragment.TAG).commit();
    }
}
