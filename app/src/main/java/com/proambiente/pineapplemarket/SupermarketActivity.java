package com.proambiente.pineapplemarket;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.inputmethod.InputMethodManager;

import com.mancj.materialsearchbar.MaterialSearchBar;
import com.proambiente.pineapplemarket.model.SearchableElement;
import com.proambiente.pineapplemarket.repository.DataRepository;
import com.proambiente.pineapplemarket.ui.home.SearchableElementsAdapter;
import com.proambiente.pineapplemarket.ui.main.ExploreFragment;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SupermarketActivity extends AppCompatActivity {
    private static final int MAX_SEARCH_TEXT_LENGTH = 50;
    private ExploreFragment fragment;
    private List<SearchableElement> localDataSource = new ArrayList<>();
    private RecyclerView recyclerSearch;
    private SearchableElementsAdapter adapter;
    private MaterialSearchBar bar;
    private DataRepository repository;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        bar.enableSearch();
        bar.requestFocus();
        bar.callOnClick();
        bar.showSuggestionsList();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.supermarket_activity);

        repository = DataRepository.getInstance(getApplication());

        fragment = new ExploreFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.map_supermarket_activity, fragment)
                .commit();

        recyclerSearch = findViewById(R.id.search_suggestions_supermarkets);
        recyclerSearch.setLayoutManager(new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false));

        adapter = new SearchableElementsAdapter(localDataSource);
        recyclerSearch.setAdapter(adapter);

        bar = findViewById(R.id.search_bar_supermarket);
        bar.setCardViewElevation(10);
        bar.callOnClick();
        bar.setNavButtonEnabled(true);
        bar.setMenuIcon(R.drawable.ic_action_back);
        bar.setArrowIcon(R.drawable.ic_action_back);
        bar.refreshDrawableState();

        bar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() >= MAX_SEARCH_TEXT_LENGTH) {
                    CharSequence sq = s.subSequence(0, MAX_SEARCH_TEXT_LENGTH - 1);
                    s.clear();
                    s.insert(0, sq);
                }
            }
        });

        bar.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener() {
            @Override
            public void onSearchStateChanged(boolean enabled) {

            }

            @Override
            public void onSearchConfirmed(CharSequence text) {
                bar.clearFocus();
                InputMethodManager in = (InputMethodManager) SupermarketActivity.this.getSystemService(getBaseContext().INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(bar.getWindowToken(), 0);
                fragment.findSupermarkets(text.toString());
            }

            @Override
            public void onButtonClicked(int buttonCode) {
                switch (buttonCode) {
                    case MaterialSearchBar.BUTTON_NAVIGATION:
                        SupermarketActivity.this.onBackPressed();
                        break;
                    case MaterialSearchBar.BUTTON_BACK:
                        SupermarketActivity.this.onBackPressed();
                        break;

                }
            }
        });

        repository.loadAllProducts(10).observe(this, products -> {
            assert products != null;
            displaySearchableElements(products);
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (requestCode == fragment.MY_PERMISSIONS_REQUEST_LOCATION) {
            fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void displaySearchableElements(List elements) {
        localDataSource = elements;
        adapter.setItems(localDataSource);
    }
}
