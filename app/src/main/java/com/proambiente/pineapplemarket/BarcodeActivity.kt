package com.proambiente.pineapplemarket

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.vision.barcode.Barcode
import com.proambiente.pineapplemarket.ui.products.ProductViewModel

class BarcodeActivity : AppCompatActivity() {
    private val RC_BARCODE = 475;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_barcode)
        startBarcodeActivity()
    }

    private fun startBarcodeActivity() {
        val i = Intent(this, BarcodeCaptureActivity::class.java)
        i.putExtra(BarcodeCaptureActivity.AutoFocus, true);
        i.putExtra(BarcodeCaptureActivity.UseFlash, false)
        startActivityForResult(i, RC_BARCODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_BARCODE) {
            when (resultCode) {
                CommonStatusCodes.SUCCESS -> {
                    if (data == null) {
                        finish()
                        return
                    }

                    val barcode = data.getParcelableExtra<Barcode>(
                            BarcodeCaptureActivity.BarcodeObject)

                    val viewModel = ViewModelProviders.of(this).get(ProductViewModel::class.java)

                    viewModel.getProduct(barcode.displayValue)
                            .observe(this, Observer {
                                if (it != null) {
                                    val i = Intent(this, ProductActivity::class.java)
                                    i.putExtra(ProductActivity.ARG_PRODUCT, it)
                                    startActivity(i)
                                } else {
                                    Toast.makeText(this,
                                            getString(R.string.product_not_found_warning),
                                            Toast.LENGTH_LONG).show()
                                }
                                finish()
                            })
                }
                else -> {
                }
            }
        }
    }
}
