package com.proambiente.pineapplemarket

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.TargetApi
import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.material.snackbar.Snackbar
import com.proambiente.pineapplemarket.repository.DataRepository
import com.proambiente.pineapplemarket.ui.home.ConnectivityViewModel
import com.proambiente.pineapplemarket.ui.login.LoginEmailFragment
import com.proambiente.pineapplemarket.ui.login.MainScreenLoginFragment
import com.proambiente.pineapplemarket.utils.NetworkChangeReceiver

class LoginActivity : AppCompatActivity(), MainScreenLoginFragment.OnButtonClicked,
        LoginEmailFragment.OnButtonClicked, LoginEmailFragment.OnResultObtained {

    private val RC_SIGN_IN: Int = 654

    private val progressBar: ProgressBar by lazy {
        findViewById<ProgressBar>(R.id.login_progress)
    }

    private val repository: DataRepository by lazy {
        DataRepository.getInstance(application)
    }

    private val viewSnackbar: View by lazy {
        findViewById(R.id.view_snackbar) as View
    }

    private val snackbar: Snackbar by lazy {
        Snackbar.make(viewSnackbar, getString(R.string.no_network_connection),
                Snackbar.LENGTH_INDEFINITE)
    }

    override fun onResult(result: Int) {
        setResult(result, Intent())
        finish()
    }

    override fun showProgressBar(show: Boolean) {
        showProgress(show)
    }

    private fun attemptLoginWithGoogle() {
        val options = GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.key_google_signin))
                .requestEmail()
                .requestProfile()
                .build()

        val googleIntent = GoogleSignIn.getClient(this, options).signInIntent
        startActivityForResult(googleIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            RC_SIGN_IN -> {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                try {
                    repository.performSignInWithGoogle(task.getResult(ApiException::class.java))
                            .addOnCompleteListener { task1 ->
                                if (task1.isSuccessful) {
                                    this.onResult(Activity.RESULT_OK)
                                } else {
                                    Log.e("LoginActivity", "Google login failed", task1.exception)
                                }
                            }
                } catch (e: Exception) {
                    Log.e("LoginActivity", "Google login failed", e)
                    Toast.makeText(this, "Google login failed, try later",
                            Toast.LENGTH_LONG).show()
                } catch (e: ApiException) {
                    Log.e("LoginActivity", "Google login failed", e)
                    Toast.makeText(this, "Google login failed, try later",
                            Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun onGoogleClicked() {
        attemptLoginWithGoogle()
    }

    override fun onEmailClicked() {
        replaceFragment(LoginEmailFragment())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        replaceFragment(MainScreenLoginFragment())

        registerReceiver(NetworkChangeReceiver(),
                IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))

        val viewModel = ViewModelProviders.of(this)
                .get(ConnectivityViewModel::class.java)

        viewModel.connected.observe(this, Observer { connected ->
            if (connected) {
                snackbar.dismiss()
                viewSnackbar.visibility = View.GONE
            } else {
                snackbar.show()
                viewSnackbar.visibility = View.VISIBLE
            }
        })
    }

    private fun replaceFragment(fragment: androidx.fragment.app.Fragment) {
        supportFragmentManager.beginTransaction().replace(
                R.id.login_content, fragment
        ).commit()
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private fun showProgress(show: Boolean) {
        val shortAnimTime = resources.getInteger(android.R.integer.config_shortAnimTime)
        progressBar.visibility = if (show) View.VISIBLE else View.INVISIBLE

        progressBar.animate().setDuration(shortAnimTime.toLong()).alpha((if (show) 1 else 0).toFloat())
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        progressBar.visibility = if (show) View.VISIBLE else View.GONE
                    }
                })
    }
}

