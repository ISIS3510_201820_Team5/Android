package com.proambiente.pineapplemarket

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.proambiente.pineapplemarket.repository.DataRepository

class SplashActivity : AppCompatActivity() {
    private lateinit var repository: DataRepository
    private lateinit var listener: FirebaseAuth.AuthStateListener
    private val SIGN_IN_CODE = 666

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.AppTheme_SplashTheme)

        repository = DataRepository.getInstance(application)
        listener = FirebaseAuth.AuthStateListener { firebaseAuth ->
            val user = firebaseAuth.currentUser
            if (user == null) {
                startActivityForResult(
                        Intent(this, LoginActivity::class.java),
                        SIGN_IN_CODE)
            } else {
                startMainActivity()
            }
        }
    }

    private fun startMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    override fun onResume() {
        super.onResume()
        repository.addStateListener(listener)
    }

    override fun onPause() {
        super.onPause()
        repository.removeStateListener(listener)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SIGN_IN_CODE) {
            when {
                resultCode == Activity.RESULT_OK -> repository.addNewUser(repository.currentUser)
                else -> finish()
            }
        } else {
            startMainActivity()
        }
    }
}