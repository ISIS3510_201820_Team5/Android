package com.proambiente.pineapplemarket.ui.list;

import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Pair;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import com.proambiente.pineapplemarket.R;
import com.proambiente.pineapplemarket.model.ListProduct;
import com.proambiente.pineapplemarket.repository.DataRepository;
import com.proambiente.pineapplemarket.ui.products.ProductViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class ProductsListFragment extends Fragment {
    public static final String TAG = ProductsListFragment.class.getCanonicalName();
    protected ProductViewModel productViewModel;
    protected ProductsListAdapter adapter;
    protected ListView listView;
    private String listId, listName;
    private Set<String> selectedItems;
    private boolean selectedAll = false;

    public static ProductsListFragment newInstance(String listId, String listName) {
        Bundle args = new Bundle();
        args.putString(UserListAdapterVertical.ARG_LIST_ID, listId);
        args.putString(UserListAdapterVertical.ARG_LIST_NAME, listName);
        ProductsListFragment fragment = new ProductsListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (selectedItems == null) {
            selectedItems = new HashSet<>();
        }

        View view = inflater.inflate(R.layout.fragment_list_products, container, false);

        adapter = new ProductsListAdapter(getContext(), new ArrayList<>());

        listView = view.findViewById(R.id.products_list_view);
        listView.setAdapter(adapter);
        listView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                String productId = ((ListProduct) adapter.getItem(position)).getId();

                if (checked)
                    selectedItems.add(productId);
                else
                    selectedItems.remove(productId);

                mode.setTitle(selectedItems.size() +
                        getString(R.string.items_selected_title));
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                MenuInflater menuInflater = mode.getMenuInflater();
                menuInflater.inflate(R.menu.toolbar_products, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_delete:
                        deleteSelectedItems();
                        mode.finish();
                        return true;
                    case R.id.action_select_all:
                        selectAllItems();
                        selectedAll = !selectedAll;
                        return true;
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                selectedItems.clear();
            }
        });

        return view;
    }

    private void selectAllItems() {
        if (selectedAll) {
            for (int i = 0; i < adapter.getCount(); i++) {
                listView.setItemChecked(i, false);
            }
        } else {
            for (int i = 0; i < adapter.getCount(); i++) {
                listView.setItemChecked(i, true);
            }
        }
    }

    private void deleteSelectedItems() {
        new DeleteSelectedItemsTask(DataRepository.getInstance(getActivity().getApplication()))
                .execute(new Pair<>(listId, selectedItems.toArray()));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle args = getArguments();
        this.listId = args.getString(UserListAdapterVertical.ARG_LIST_ID);

        productViewModel = ViewModelProviders.of(this).get(ProductViewModel.class);
        productViewModel.getListProducts(listId)
                .observe(this, products1 -> adapter.setItems(products1, listId));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        HashMap<ListProduct, Boolean> modified = adapter.getModifiedProducts();
        if (modified != null && modified.size() > 0)
            new UpdateProductListsTask(DataRepository.getInstance(getActivity().getApplication()))
                    .execute(new Pair<>(listId, modified));
    }
}
