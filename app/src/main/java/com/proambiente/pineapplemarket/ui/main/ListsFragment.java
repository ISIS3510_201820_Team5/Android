package com.proambiente.pineapplemarket.ui.main;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.proambiente.pineapplemarket.R;
import com.proambiente.pineapplemarket.ui.list.VerticalListsFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

public class ListsFragment extends Fragment {
    private ListsViewModel mViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lists_fragment, container, false);

        FloatingActionButton btn = view.findViewById(R.id.btn_add_list_2);
        btn.setOnClickListener(v -> {
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
            View mView = getLayoutInflater().inflate(R.layout.dialog_add_list, null);
            final EditText nName = mView.findViewById(R.id.dialog_add_list_name);
            Button mLogin = mView.findViewById(R.id.dialog_add_list_button);

            mBuilder.setView(mView);
            AlertDialog dialog = mBuilder.create();

            mLogin.setOnClickListener(view1 -> {
                if (!nName.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(),
                            R.string.dialog_add_list_successful,
                            Toast.LENGTH_SHORT).show();
                    mViewModel.putUserList(nName.getText().toString());
                    dialog.dismiss();
                } else {
                    Toast.makeText(getActivity(),
                            R.string.dialog_add_list_error,
                            Toast.LENGTH_SHORT).show();
                }
            });

            dialog.show();
        });
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ListsViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getFragmentManager() == null) return;

        final VerticalListsFragment fragment = new VerticalListsFragment();
        getChildFragmentManager().beginTransaction()
                .replace(R.id.main_user_list_view_vertical, fragment).commit();
    }
}
