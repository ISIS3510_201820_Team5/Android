package com.proambiente.pineapplemarket.ui.list;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.proambiente.pineapplemarket.R;
import com.proambiente.pineapplemarket.model.List;

import java.util.ArrayList;


public class UserListAdapter extends RecyclerView.Adapter {

    private ArrayList<List> items = new ArrayList<>();

    public UserListAdapter() {
    }

    public void loadItems(ArrayList<List> newItems) {
        items = newItems;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_user_lists_item, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((ListViewHolder) holder).bindView((position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mItemText;
        private TextView mItemText2;
        private String owner;
        private String noOwner;

        public ListViewHolder(View itemView) {
            super(itemView);
            mItemText = itemView.findViewById(R.id.titleListUser);
            mItemText2 = itemView.findViewById(R.id.products_resume);
            itemView.setOnClickListener(this);
            owner = itemView.getContext().getString(R.string.owner);
            noOwner = itemView.getContext().getString(R.string.no_owner);
        }

        public void bindView(int position) {
            mItemText.setText(items.get(position).getName());
            String isOwner = items.get(position).isOwner() ? owner : noOwner;
            mItemText2.setText(isOwner);
        }

        public void onClick(View view) {

        }
    }

}
