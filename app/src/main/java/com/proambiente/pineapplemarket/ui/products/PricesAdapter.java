package com.proambiente.pineapplemarket.ui.products;

import android.content.res.Resources;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.proambiente.pineapplemarket.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class PricesAdapter extends RecyclerView.Adapter {
    private List<Pair<String, Double>> prices;

    public PricesAdapter(@NonNull List<Pair<String, Double>> prices,
                         @NonNull Resources resources) {
        setPrices(prices, resources);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.markets_list_item, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Pair<String, Double> p = prices.get(position);
        ((SimpleViewHolder) holder).bindView(p.first,
                (p.second < 0) ? "" : p.second.toString());
    }

    @Override
    public int getItemCount() {
        if (prices == null)
            return 0;
        return prices.size();
    }

    public void setPrices(@NonNull List<Pair<String, Double>> prices, Resources resources) {
        if (prices.size() == 0)
            prices.add(new Pair<>(resources.getString(
                    R.string.no_prices_available_now_label), -1d));
        this.prices = prices;
        this.notifyDataSetChanged();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        private TextView title, description;

        SimpleViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.text1);
            description = itemView.findViewById(R.id.text2);
        }

        void bindView(String title, String description) {
            StringBuilder sb = new StringBuilder(title);
            sb.replace(0, 1, sb.substring(0, 1).toUpperCase());
            this.title.setText(sb.toString());
            this.description.setText(description);
        }
    }
}
