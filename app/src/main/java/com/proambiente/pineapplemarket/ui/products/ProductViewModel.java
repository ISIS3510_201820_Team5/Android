package com.proambiente.pineapplemarket.ui.products;

import android.app.Application;
import android.util.Pair;

import com.proambiente.pineapplemarket.model.ListProduct;
import com.proambiente.pineapplemarket.model.Product;
import com.proambiente.pineapplemarket.repository.DataRepository;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class ProductViewModel extends AndroidViewModel {
    private DataRepository repository;

    public ProductViewModel(Application application) {
        super(application);
        repository = DataRepository.getInstance(application);
    }

    public LiveData<List<Product>> getProducts(int limit) {
        return repository.loadAllProducts(limit);
    }

    public MutableLiveData<List<List<Pair<String, Double>>>> getPricesByProduct(Product product) {
        MutableLiveData<List<List<Pair<String, Double>>>> prices = new MutableLiveData<>();

        if (product.getPrices() == null) {
            prices = repository.getPricesByProduct(product.getId());
        }

        return prices;
    }

    public MutableLiveData<List<List<Pair<String, Double>>>> getHistoricalPricesByProduct(Product product) {
        MutableLiveData<List<List<Pair<String, Double>>>> prices = new MutableLiveData<>();

        if (product.getPrices() == null) {
            prices = repository.getHistoricalPricesByProduct(product.getId());
        }

        return prices;
    }

    public LiveData<List<ListProduct>> getListProducts(String id) {
        return repository.getProductsInList(id);
    }

    public MutableLiveData<Product> getProduct(@NotNull String displayValue) {
        return repository.getProduct(displayValue);
    }
}
