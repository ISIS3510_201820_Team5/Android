package com.proambiente.pineapplemarket.ui.main;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import com.proambiente.pineapplemarket.model.List;
import com.proambiente.pineapplemarket.repository.DataRepository;

public class ListsViewModel extends AndroidViewModel {
    private DataRepository repository;

    public ListsViewModel(@NonNull Application application) {
        super(application);
        repository = DataRepository.getInstance(application);
    }

    public LiveData<java.util.List<List>> getAllLists() {
        return repository.getAllLists();
    }

    public void putUserList(String name) {
        repository.addList(name);
    }
}
