package com.proambiente.pineapplemarket.ui.list;

import android.app.AlertDialog;
import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.proambiente.pineapplemarket.R;
import com.proambiente.pineapplemarket.model.List;
import com.proambiente.pineapplemarket.ui.main.ListsViewModel;

import java.util.ArrayList;

public class UserListsFragment extends Fragment {
    public static final String TAG = UserListsFragment.class.getCanonicalName();
    private ListsViewModel mViewModel;
    private UserListAdapter listAdapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_lists, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.user_list_view);
        listAdapter = new UserListAdapter();

        recyclerView.setAdapter(listAdapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);

        FloatingActionButton btn = view.findViewById(R.id.btn_add_list_1);
        btn.setOnClickListener(v -> {
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
            View mView = getLayoutInflater().inflate(R.layout.dialog_add_list, null);
            final EditText nName = mView.findViewById(R.id.dialog_add_list_name);
            Button mLogin = mView.findViewById(R.id.dialog_add_list_button);

            mBuilder.setView(mView);
            AlertDialog dialog = mBuilder.create();

            mLogin.setOnClickListener(view1 -> {
                if (!nName.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(),
                            R.string.dialog_add_list_successful,
                            Toast.LENGTH_SHORT).show();
                    mViewModel.putUserList(nName.getText().toString());
                    dialog.dismiss();
                } else {
                    Toast.makeText(getActivity(),
                            R.string.dialog_add_list_error,
                            Toast.LENGTH_SHORT).show();
                }
            });

            dialog.show();
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ListsViewModel.class);

        mViewModel.getAllLists().observe(this, userList1 ->
                listAdapter.loadItems((ArrayList<List>) userList1));
    }
}

