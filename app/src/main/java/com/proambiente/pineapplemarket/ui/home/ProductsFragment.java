package com.proambiente.pineapplemarket.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.proambiente.pineapplemarket.R;
import com.proambiente.pineapplemarket.model.SearchableElement;
import com.proambiente.pineapplemarket.ui.products.ProductViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ProductsFragment extends Fragment {
    public static final String TAG = ProductsFragment.class.getCanonicalName();
    private ProductViewModel productViewModel;
    private SearchableElementsAdapter searchableElementsAdapter;
    private RecyclerView recyclerView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_products, container, false);

        recyclerView = view.findViewById(R.id.products_recyclerview);
        RecyclerView.LayoutManager layoutManager;
        layoutManager = new GridLayoutManager(getActivity(), 2, GridLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        searchableElementsAdapter = new SearchableElementsAdapter(new ArrayList<>());
        recyclerView.setAdapter(searchableElementsAdapter);

        /*SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView);*/

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        productViewModel = ViewModelProviders.of(this).get(ProductViewModel.class);

        productViewModel.getProducts(10).observe(this, products1 -> {
            searchableElementsAdapter.setItems((List<SearchableElement>) (List<?>) products1);
        });
    }
}
