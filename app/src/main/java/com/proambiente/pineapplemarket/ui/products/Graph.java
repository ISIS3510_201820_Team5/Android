package com.proambiente.pineapplemarket.ui.products;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.proambiente.pineapplemarket.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Graph extends Fragment {

    private LineChart chart;
    private TextView mItemText;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_graph, container, false);

        mItemText = view.findViewById(R.id.text_graph);

        chart = view.findViewById(R.id.lineChart);

        chart.getDescription().setEnabled(false);
        chart.setDrawGridBackground(false);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(true);

        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setLabelCount(5, false);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setLabelCount(5, false);
        rightAxis.setDrawGridLines(false);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        chart.animateX(750);

        return view;
    }

    public void generateDataLine(List<List<Pair<String,Double>>> pairs){

        ArrayList<Entry> values1 = new ArrayList<>();
        ArrayList<String> dates = new ArrayList<>();

        float min = 50000;
        float max = 0;
        double average = 0;
        int i =0;
        for(List<Pair<String,Double>> pair : pairs){
            float price = pair.get(0).second.floatValue();
            values1.add(new Entry(i, price));
            dates.add(pair.get(0).first);
            if(price < min) min = price;
            if(price > max) max = price;
            average += price;
            i++;
        }

        if(pairs.size() > 0 ){
            average /= pairs.size();
            if(pairs.get(pairs.size()-1).get(0).second <= average)
                mItemText.setText(getContext().getResources().getString(R.string.text_graph_buy));
            else
                mItemText.setText(getContext().getResources().getString(R.string.text_graph_not_buy));
        }
        else{
            mItemText.setText(getContext().getResources().getString(R.string.text_graph_no_data));
        }

        LineDataSet d1 = new LineDataSet(values1, getResources().getString(R.string.historical_prices));
        d1.setColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
        d1.setCircleColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
        d1.setLineWidth(2.5f);
        d1.setCircleRadius(4.5f);
        d1.setHighLightColor(ContextCompat.getColor(getContext(), R.color.navigationBarColor));
        d1.setDrawValues(false);

        ArrayList<ILineDataSet> sets = new ArrayList<>();
        sets.add(d1);

        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setAxisMinimum(min);
        leftAxis.setAxisMaximum(max);

        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setAxisMinimum(min);
        rightAxis.setAxisMaximum(max);

        XAxis xAxis = chart.getXAxis();
        String[] weekArr = getDate(dates);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return weekArr[(int) value];
        }});

        chart.setData(new LineData(sets));
    }

    public String[] getDate(ArrayList<String> dates)
    {
        String dateFormat = "dd/MM/yy";
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        String[] datesArr = new String[dates.size()];

        for(int i = 0; i < dates.size();i++){
            long milliSeconds = Long.parseLong(dates.get(i));
            calendar.setTimeInMillis(milliSeconds);
            datesArr[i] = formatter.format(calendar.getTime());
        }
        return datesArr;
    }
}

