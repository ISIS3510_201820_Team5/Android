package com.proambiente.pineapplemarket.ui.list;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.proambiente.pineapplemarket.ListActivity;
import com.proambiente.pineapplemarket.R;
import com.proambiente.pineapplemarket.model.List;

import java.util.ArrayList;

public class UserListAdapterVertical extends RecyclerView.Adapter {
    public static final String ARG_LIST_ID = "ARG_LIST_ID";
    public static final String ARG_LIST_NAME = "ARG_LIST_NAME";
    private ArrayList<List> items = new ArrayList<>();

    public UserListAdapterVertical() {
    }

    public void loadItems(ArrayList<List> newItems) {
        items = newItems;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.fragment_user_list_vertical_item, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((ListViewHolder) holder).bindView((position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private class ListViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        private TextView mItemText;
        private String id, name;

        ListViewHolder(View itemView) {
            super(itemView);
            mItemText = itemView.findViewById(R.id.titleListUserVertical);
            itemView.setOnClickListener(this);
        }

        void bindView(int position) {
            mItemText.setText(items.get(position).getName());
            id = items.get(position).getId();
            name = items.get(position).getName();
        }

        @Override
        public void onClick(View v) {
            Context context = v.getContext();
            Intent intent = new Intent(context, ListActivity.class);
            intent.putExtra(ARG_LIST_ID, id);
            intent.putExtra(ARG_LIST_NAME, name);
            context.startActivity(intent);
        }
    }
}
