package com.proambiente.pineapplemarket.ui.home;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ConnectivityViewModel extends ViewModel {
    private MutableLiveData<Boolean> connected = new MutableLiveData<>();

    public ConnectivityViewModel() {
        connected.setValue(true);
    }

    public MutableLiveData<Boolean> getConnected() {
        return connected;
    }

    public void postConnected(Boolean connected) {
        this.connected.postValue(connected);
    }
}
