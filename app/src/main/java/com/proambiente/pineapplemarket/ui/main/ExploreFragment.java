package com.proambiente.pineapplemarket.ui.main;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.proambiente.pineapplemarket.R;
import com.proambiente.pineapplemarket.ui.map.NearByPlacesData;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;


public class ExploreFragment extends Fragment implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private final static int ZOOM = 14;
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";
    private GoogleMap mGoogleMap;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private SupportMapFragment mapFragment;
    private int PROXIMITY_RADIUS = 1000;

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        if (mGoogleMap != null) {
            outState.putParcelable(KEY_CAMERA_POSITION, mGoogleMap.getCameraPosition());
            outState.putParcelable(KEY_LOCATION, mLastLocation);
            super.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        if (mGoogleMap == null) {
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            loadPlaces();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity() == null) return;
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mGoogleMap.setMyLocationEnabled(true);
                loadPlaces();
                if (mLastLocation != null) {
                    moveCamera();
                }
            } else {
                checkLocationPermission();
            }
        } else {
            buildGoogleApiClient();
            mGoogleMap.setMyLocationEnabled(true);
            loadPlaces();
            if (mLastLocation != null) {
                moveCamera();
            }
        }
    }

    private synchronized void buildGoogleApiClient() {
        if (getActivity() == null) return;
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (getActivity() != null && ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.requestLocationUpdates(
                        mGoogleApiClient, mLocationRequest, this);

                if (mLastLocation != null) {
                    loadPlaces();
                    moveCamera();
                }
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {
        Location last = mLastLocation;
        mLastLocation = location;

        if (last != null && mLastLocation != null) {
            if (Math.abs(last.getLatitude() - mLastLocation.getLatitude()) > 1 ||
                    Math.abs(last.getLongitude() - mLastLocation.getLongitude()) > 1) {
                loadPlaces();
                moveCamera();
            }
        }
        if (last == null && mLastLocation != null) {
            loadPlaces();
            moveCamera();
        }
    }

    private void moveCamera() {
        LatLng coordinate = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, ZOOM);
        mGoogleMap.animateCamera(yourLocation);
    }

    private void checkLocationPermission() {
        if (getActivity() != null && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.title_request_location_permissions)
                        .setMessage(R.string.message_request_location_permissions)
                        .setPositiveButton("OK", (dialogInterface, i) -> ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                MY_PERMISSIONS_REQUEST_LOCATION))
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (getActivity() != null && ContextCompat.checkSelfPermission(
                            getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mGoogleMap.setMyLocationEnabled(true);
                        loadPlaces();
                    }
                } else {
                    Toast.makeText(getActivity(), "permission denied", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mLastLocation = savedInstanceState.getParcelable(KEY_LOCATION);
        }

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            getChildFragmentManager().beginTransaction().replace(R.id.map, mapFragment).commit();
        }

        mapFragment.getMapAsync(this);

        LocationManager manager = null;
        if (getContext() != null)
            manager = (LocationManager)
                    getContext().getSystemService(Context.LOCATION_SERVICE);

        if (manager != null && !manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            Toast.makeText(getContext(), "GPS is disable! Please turn on GPS.", Toast.LENGTH_LONG).show();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.explore_fragment, container, false);
    }

    private void loadPlaces() {
        Object dataTransfer[] = new Object[2];
        NearByPlacesData getNearbyPlacesData = new NearByPlacesData();

        if (mLastLocation != null) {
            mGoogleMap.clear();
            String searchBy = "supermarket";
            String url = getUrl(mLastLocation.getLatitude(), mLastLocation
                    .getLongitude(), searchBy);
            dataTransfer[0] = mGoogleMap;
            dataTransfer[1] = url;

            getNearbyPlacesData.execute(dataTransfer);
        }
    }

    public void findSupermarkets(String textToSearch) {
        Object dataTransfer[] = new Object[2];
        NearByPlacesData getNearbyPlacesData = new NearByPlacesData();

        if (mGoogleMap != null && mLastLocation != null) {
            mGoogleMap.clear();
            mGoogleMap.clear();
            String url = getUrlFindMarket(mLastLocation.getLatitude(), mLastLocation
                    .getLongitude(), textToSearch);
            dataTransfer[0] = mGoogleMap;
            dataTransfer[1] = url;

            getNearbyPlacesData.execute(dataTransfer);
            moveCamera();
        }
    }

    private String getUrl(double latitude, double longitude, String nearbyPlace) {
        return "https://maps.googleapis.com/maps/api/place/nearbysearch/json?" +
                "location=" + latitude + "," + longitude +
                "&radius=" + PROXIMITY_RADIUS +
                "&type=" + nearbyPlace +
                "&key=AIzaSyCYZaDq9bwxMdj9_jGl_ZCNj_H96EVtvOU";
    }

    private String getUrlFindMarket(double latitude, double longitude, String text) {
        StringBuilder googlePlaceUrl = new StringBuilder(
                "https://maps.googleapis.com/maps/api/place/textsearch/json?")
                .append("query=supermercados+")
                .append(text.toLowerCase().trim().replace(" ", "+"))
                .append("&location=").append(latitude).append(",").append(longitude)
                .append("&radius=").append(PROXIMITY_RADIUS)
                .append("&key=AIzaSyCYZaDq9bwxMdj9_jGl_ZCNj_H96EVtvOU");

        Log.e("URL", googlePlaceUrl + "");
        return googlePlaceUrl.toString();
    }
}
