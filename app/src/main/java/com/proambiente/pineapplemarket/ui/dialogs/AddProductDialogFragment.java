package com.proambiente.pineapplemarket.ui.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import com.proambiente.pineapplemarket.R;
import com.proambiente.pineapplemarket.model.Product;
import com.proambiente.pineapplemarket.repository.DataRepository;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

public class AddProductDialogFragment extends DialogFragment {
    private static final String ARGS_PRODUCT = "ARGS_PRODUCT";
    private static final String ARGS_LISTS_NAMES = "ARGS_LISTS_NAMES";
    private static final String ARGS_LISTS_IDS = "ARGS_LISTS_IDS";
    private String[] listIds;
    private ArrayList mSelectedItems;
    private DataRepository repository;
    private Product product;
    private OnResult listener;

    public AddProductDialogFragment() {
    }

    public static AddProductDialogFragment newInstance(String[] names,
                                                       String[] ids,
                                                       Product product) {
        Bundle args = new Bundle();
        args.putSerializable(ARGS_PRODUCT, product);
        args.putStringArray(ARGS_LISTS_NAMES, names);
        args.putStringArray(ARGS_LISTS_IDS, ids);

        AddProductDialogFragment fragment = new AddProductDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        listener = (OnResult) getActivity();
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (getActivity() != null)
            repository = DataRepository.getInstance(getActivity().getApplication());

        Bundle args = getArguments();

        String[] listNames = new String[0];

        if (args != null) {
            listNames = args.getStringArray(ARGS_LISTS_NAMES);
            listIds = args.getStringArray(ARGS_LISTS_IDS);
            product = (Product) args.getSerializable(ARGS_PRODUCT);
        }

        mSelectedItems = new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        if (listNames == null || listNames.length == 0) {
            builder.setMessage(R.string.dialog_pick_list_empty)
                    .setPositiveButton(getString(R.string.ok), (dialog, id) -> {
                    });
        } else {
            builder.setTitle(R.string.dialog_pick_lists)
                    .setMultiChoiceItems(listNames, null,
                            (dialog, which, isChecked) -> {
                                if (isChecked) {
                                    mSelectedItems.add(which);
                                } else if (mSelectedItems.contains(which)) {
                                    mSelectedItems.remove(Integer.valueOf(which));
                                }
                            })
                    .setPositiveButton("Agregar", (dialog, id) -> {
                        for (int i = 0; i < mSelectedItems.size(); i++) {
                            int num = (int) mSelectedItems.get(i);
                            repository.addProductToList(product, listIds[num], 1)
                                    .observe(getActivity(), added ->
                                            listener.productAddedInList(added));
                        }
                    })
                    .setNegativeButton("Cancelar", (dialog, id) -> {
                    });
        }
        return builder.create();
    }

    public interface OnResult {
        void productAddedInList(boolean added);
    }
}
