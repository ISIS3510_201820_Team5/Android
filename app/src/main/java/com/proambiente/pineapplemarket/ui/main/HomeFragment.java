package com.proambiente.pineapplemarket.ui.main;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.proambiente.pineapplemarket.R;
import com.proambiente.pineapplemarket.ui.home.ProductsFragment;
import com.proambiente.pineapplemarket.ui.list.UserListsFragment;

public class HomeFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getFragmentManager() == null) return;

        UserListsFragment usersListsFragment = new UserListsFragment();
        getChildFragmentManager().beginTransaction()
                .replace(R.id.main_user_list, usersListsFragment,
                        UserListsFragment.TAG).commit();

        ProductsFragment productsFragment = new ProductsFragment();
        getChildFragmentManager().beginTransaction()
                .replace(R.id.main_products_list, productsFragment,
                        ProductsFragment.TAG).commit();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
