package com.proambiente.pineapplemarket.ui.list;

import android.os.AsyncTask;
import android.util.Pair;

import com.proambiente.pineapplemarket.repository.DataRepository;

public class DeleteSelectedItemsTask extends AsyncTask<Pair<String, Object[]>, Double, Void> {
    private DataRepository repository;

    public DeleteSelectedItemsTask(DataRepository repository) {
        this.repository = repository;
    }

    @Override
    protected Void doInBackground(Pair<String, Object[]>... pairs) {
        if (pairs == null || pairs[0] == null)
            return null;

        if (pairs[0].first == null || pairs[0].second == null)
            return null;

        String product;
        String listId = pairs[0].first;
        Object[] list = pairs[0].second;

        for (int i = 0; i < list.length; i++) {
            product = list[i].toString();
            repository.deleteProductInList(listId, product);
        }

        return null;
    }
}
