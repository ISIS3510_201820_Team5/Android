package com.proambiente.pineapplemarket.ui.list;

import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.proambiente.pineapplemarket.R;
import com.proambiente.pineapplemarket.model.List;
import com.proambiente.pineapplemarket.ui.main.ListsViewModel;

import java.util.ArrayList;

public class VerticalListsFragment extends Fragment {
    private UserListAdapterVertical listAdapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_list_vertical, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.user_list_view_vertical);
        listAdapter = new UserListAdapterVertical();
        recyclerView.setAdapter(listAdapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ListsViewModel mViewModel = ViewModelProviders.of(this).get(ListsViewModel.class);
        mViewModel.getAllLists().observe(this, userList1 -> {
            listAdapter.loadItems((ArrayList<List>) userList1);
        });
    }
}
