package com.proambiente.pineapplemarket.ui.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class LoginViewModel : ViewModel() {
    val emailClicked: MutableLiveData<Boolean> = MutableLiveData()
}
