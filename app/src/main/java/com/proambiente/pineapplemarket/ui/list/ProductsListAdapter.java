package com.proambiente.pineapplemarket.ui.list;

import android.content.Context;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.proambiente.pineapplemarket.R;
import com.proambiente.pineapplemarket.model.ListProduct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ProductsListAdapter extends ArrayAdapter {

    public final static String URL = "https://us-central1-pina-9922f.cloudfunctions.net/createPurchasePlan";
    private static final String TAG = ProductsListAdapter.class.getCanonicalName();
    public HashMap<String, Double> totalPrices;
    private Context context;
    private List<ListProduct> products;
    private String currentList;
    // second argument is a flag, deleted or not
    private HashMap<ListProduct, Boolean> modifiedProducts;
    private StringBuilder tagsBuilder;
    private RequestOptions options;
    private HashMap<String, Pair<String, Double>> productMarketsPrices;


    public ProductsListAdapter(@NonNull Context context, @NonNull List<ListProduct> products) {
        super(context, 0, products);
        this.context = context;
        this.products = products;
        tagsBuilder = new StringBuilder();
        modifiedProducts = new HashMap<>();

        totalPrices = new HashMap<>();
        productMarketsPrices = new HashMap<>();

        options = new RequestOptions()
                .placeholder(R.drawable.generic_product)
                .error(R.drawable.generic_product)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .priority(Priority.HIGH);
    }

    public HashMap<ListProduct, Boolean> getModifiedProducts() {
        return modifiedProducts;
    }

    public void setItems(List<ListProduct> products, String listId) {
        currentList = listId;
        this.products = products;
        sendRequest();
        notifyDataSetChanged();
    }

    public void sendRequest() {
        Map<String, String> jsonParams = new HashMap<>();
        jsonParams.put("listId", currentList);

        JSONObject parameters = new JSONObject(jsonParams);

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, URL, parameters, response -> {
            try {
                Log.e("RESP========", response.toString());
                Iterator<String> keys = response.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    if (key.equals("total"))
                        totalPrices.put(key, response.getDouble(key));
                    else {
                        JSONObject marketInfo = new JSONObject(response.getString(key));
                        Iterator<String> marketKeys = response.keys();
                        while (marketKeys.hasNext()) {
                            String market = marketKeys.next();
                            if (market.equals("total"))
                                totalPrices.put(market, marketInfo.getDouble(market));
                            else {
                                JSONArray arr = marketInfo.getJSONArray("products");

                                for (int i = 0; i < arr.length(); i++) {
                                    String barcode = arr.getJSONObject(i).getString("barcode");
                                    Double bestPrice = arr.getJSONObject(i).getDouble("price");
                                    productMarketsPrices.put(barcode, new Pair<>(market, bestPrice));
                                }
                            }
                        }
                    }
                }
                for (ListProduct p : products) {
                    String id = p.getId();
                    String bestMarket = productMarketsPrices.get(id).first;
                    Double bestPrice = productMarketsPrices.get(id).second;
                    p.setBestMarketName(bestMarket);
                    p.setBestPrice(bestPrice);

                    Log.e("PRODUCT===", p.getName() + " " + p.getBestPrice());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> error.printStackTrace());
        Volley.newRequestQueue(getContext()).add(jsonRequest);
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater == null)
            return convertView;

        convertView = inflater.inflate(R.layout.product_list_item, parent, false);

        ListProduct product = products.get(position);
        TextView name = convertView.findViewById(R.id.name_product_list);
        name.setText(product.getName());

        TextView bestMarket = convertView.findViewById(R.id.best_market);
        String textMarket = product.getBestMarketName() == null ? " " : product.getBestMarketName();
        bestMarket.setText(getContext().getResources().getString(R.string.lower_prices_in) + " " + textMarket);

        TextView bestPrice = convertView.findViewById(R.id.best_price);
        bestPrice.setText("$ " + product.getBestPrice());

        TextView tags = convertView.findViewById(R.id.tags_product_list);
        if (product.getTags() == null) {
            tags.setText(R.string.message_product_without_tags);
        } else {
            tagsBuilder.setLength(0);
            for (int i = 0; i < product.getTags().size(); i++) {
                tagsBuilder.append(product.getTags().get(i));
                if (i != product.getTags().size() - 1) {
                    tagsBuilder.append(", ");
                }
            }

            tags.setText(tagsBuilder);
        }

        ImageView image = convertView.findViewById(R.id.image_product_list);
        Glide.with(context).load(product.getImage()).apply(options).into(image);

        Button plus = convertView.findViewById(R.id.btn_plus_quantity);
        Button minus = convertView.findViewById(R.id.btn_minus_quantity);

        TextView quantity = convertView.findViewById(R.id.quantity_product_list);
        quantity.setText(String.valueOf(product.getQuantity()));

        plus.setOnClickListener(v -> {
            int q = Integer.parseInt(quantity.getText().toString());
            if (q < 10) {
                quantity.setText(String.valueOf(q + 1));
                addProductToModifiedList(product, q + 1);
            }
        });

        minus.setOnClickListener(v -> {
            int q = Integer.parseInt(quantity.getText().toString());
            if (q > 1) {
                quantity.setText(String.valueOf(q - 1));
                addProductToModifiedList(product, q - 1);
            }
        });

        return convertView;
    }

    private void addProductToModifiedList(ListProduct product, boolean deleted) {
        if (modifiedProducts.put(product, deleted) == null) {
            Log.e(TAG, "Posible error al guardar producto modificado");
        }
    }

    private void addProductToModifiedList(ListProduct product, @Nullable int quantity) {
        product.setQuantity(quantity);
        if (!modifiedProducts.containsKey(product)) {
            modifiedProducts.put(product, false);
        }
    }

    @Override
    public int getCount() {
        return products.size();
    }
}
