package com.proambiente.pineapplemarket.ui.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.proambiente.pineapplemarket.ProductActivity;
import com.proambiente.pineapplemarket.R;
import com.proambiente.pineapplemarket.model.Product;
import com.proambiente.pineapplemarket.model.SearchableElement;

import java.util.List;

public class SearchableElementsAdapter extends RecyclerView.Adapter {
    private Context context;
    private List<SearchableElement> items;
    private SearchableElementsListener listener;
    private RequestOptions options;

    public SearchableElementsAdapter(List<SearchableElement> newItems,
                                     SearchableElementsListener listener) {
        items = newItems;
        this.listener = listener;
    }

    public SearchableElementsAdapter(List<SearchableElement> newItems) {
        this.items = newItems;
        listener = (position, viewHolder) -> {
            Intent intent = new Intent(context, ProductActivity.class);
            Bundle args = new Bundle();
            args.putSerializable(ProductActivity.ARG_PRODUCT, items.get(position));
            intent.putExtras(args);
            context.startActivity(intent);
        };

        options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.generic_product)
                .error(R.drawable.generic_product)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .priority(Priority.HIGH);
    }

    public void setItems(List<SearchableElement> newItems) {
        items = newItems;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.products_item, parent,
                false);
        return new ElementsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((ElementsViewHolder) holder).bindView(position);
    }

    @Override
    public int getItemCount() {
        if (items == null)
            return 0;
        return items.size();
    }

    public interface SearchableElementsListener {
        void onElementClicked(int position, ElementsViewHolder viewHolder);
    }

    public class ElementsViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {
        private TextView mItemText;
        private ImageView mItemImage;

        public ElementsViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mItemImage = itemView.findViewById(R.id.elementImage);
            mItemText = itemView.findViewById(R.id.elementTitle);
        }

        public void bindView(int position) {
            mItemText.setText(items.get(position).getName());
            if (items.get(position) instanceof Product) {
                Product p = (Product) items.get(position);

                Glide.with(context).load(p.getImage())
                        .apply(options)
                        .into(mItemImage);
            }
        }

        @Override
        public void onClick(View v) {
            listener.onElementClicked(this.getAdapterPosition(), this);
        }
    }
}
