package com.proambiente.pineapplemarket.ui.login

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.proambiente.pineapplemarket.R

class MainScreenLoginFragment : Fragment() {
    interface OnButtonClicked {
        fun onEmailClicked()
        fun onGoogleClicked()
    }

    private lateinit var listener: OnButtonClicked

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as OnButtonClicked
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_login_buttons, container, false)

        val mEmailSignInButton = view.findViewById<Button>(
                R.id.email_sign_in_button)

        /*val mGoogleSignInButton = view.findViewById<Button>(
                R.id.google_sign_in_button)*/

        mEmailSignInButton.setOnClickListener {
            listener.onEmailClicked()
        }

        /*mGoogleSignInButton.setOnClickListener {
            listener.onGoogleClicked()
        }*/

        return view
    }
}