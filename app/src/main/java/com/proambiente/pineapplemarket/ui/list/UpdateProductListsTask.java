package com.proambiente.pineapplemarket.ui.list;

import android.os.AsyncTask;
import android.util.Pair;

import com.proambiente.pineapplemarket.model.ListProduct;
import com.proambiente.pineapplemarket.repository.DataRepository;

import java.util.HashMap;

public class UpdateProductListsTask extends AsyncTask<Pair<String, HashMap<ListProduct, Boolean>>, Void, Void> {
    private DataRepository repository;

    public UpdateProductListsTask(DataRepository repository) {
        this.repository = repository;
    }

    @SafeVarargs
    @Override
    protected final Void doInBackground(Pair<String, HashMap<ListProduct, Boolean>>... pairs) {
        Object keys[] = pairs[0].second.keySet().toArray();

        ListProduct product;

        for (int i = 0; i < keys.length; i++) {
            product = (ListProduct) keys[i];
            if (pairs[0].second.get(product)) {
                repository.deleteProductInList(pairs[0].first, product.getId());
            } else {
                repository.updateProductQuantityInList(pairs[0].first, product.getId(),
                        product.getQuantity());
            }
        }

        return null;
    }
}
