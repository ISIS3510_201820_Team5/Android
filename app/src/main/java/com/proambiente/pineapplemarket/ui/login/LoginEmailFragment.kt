package com.proambiente.pineapplemarket.ui.login

import android.app.Activity
import android.app.Application
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuthEmailException
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.proambiente.pineapplemarket.R
import com.proambiente.pineapplemarket.repository.DataRepository

class LoginEmailFragment : Fragment() {
    private lateinit var buttonListener: OnButtonClicked
    private lateinit var mSignInOrRegister: Button
    private lateinit var mNameLayout: TextInputLayout
    private lateinit var mNameEditText: EditText
    private lateinit var resultListener: OnResultObtained

    private val repository: DataRepository by lazy {
        DataRepository.getInstance(activity!!.applicationContext as Application)
    }

    interface OnResultObtained {
        fun onResult(result: Int)
    }

    interface OnButtonClicked {
        fun showProgressBar(show: Boolean)
    }

    override fun onStart() {
        super.onStart()
        buttonListener = activity as OnButtonClicked
        resultListener = activity as OnResultObtained
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_login_email, container, false)
        mSignInOrRegister = view.findViewById(R.id.email_sign_in_button)
        mNameLayout = view.findViewById(R.id.ly_name) as TextInputLayout
        mNameEditText = view.findViewById(R.id.name)

        mSignInOrRegister.setOnClickListener {
            attemptLogin(
                    view.findViewById(R.id.email),
                    view.findViewById(R.id.password)
            )
        }
        return view
    }

    private fun validateUserAndPassword(mEmailView: EditText, mPasswordView: EditText)
            : Pair<String?, String?> {
        mEmailView.error = null
        mPasswordView.error = null

        val email = mEmailView.text.toString()
        val password = mPasswordView.text.toString()

        var cancel = false
        var focusView: View? = null

        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.error = getString(R.string.error_invalid_password)
            focusView = mPasswordView
            cancel = true
        }

        if (TextUtils.isEmpty(email)) {
            mEmailView.error = getString(R.string.error_field_required)
            focusView = mEmailView
            cancel = true
        } else if (!isEmailValid(email)) {
            mEmailView.error = getString(R.string.error_invalid_email)
            focusView = mEmailView
            cancel = true
        }

        if (cancel) {
            focusView!!.requestFocus()
            return null to null
        }

        return email to password
    }

    private fun isEmailValid(email: String): Boolean {
        return email.contains("@")
    }

    private fun isPasswordValid(password: String): Boolean {
        return password.length >= 6
    }

    private fun changeLayoutToRegister(change: Boolean) {
        mNameLayout.visibility = if (change) View.VISIBLE else View.GONE
        mSignInOrRegister.text = if (change) getString(R.string.action_register) else getString(R.string.action_sign_in)
    }

    private fun attemptLogin(mEmailView: EditText, mPasswordView: EditText) {
        changeLayoutToRegister(false)
        val (email, password) = validateUserAndPassword(mEmailView,
                mPasswordView)
        if (email == null || password == null) return

        repository.performSignIn(email, password).addOnCompleteListener { task ->
            if (!task.isSuccessful) {
                try {
                    throw task.exception!!
                } catch (e: NullPointerException) {
                } catch (e: FirebaseAuthInvalidUserException) {
                    changeLayoutToRegister(true)
                    mSignInOrRegister.setOnClickListener {
                        attemptRegister(mEmailView, mPasswordView)
                    }
                } catch (e: FirebaseAuthInvalidCredentialsException) {
                    mPasswordView.error = getString(R.string.error_wrong_password)
                    mPasswordView.requestFocus()
                } catch (e: Exception) {
                }
            } else {
                resultListener.onResult(Activity.RESULT_OK)
            }
            buttonListener.showProgressBar(false)
        }
        buttonListener.showProgressBar(true)
    }

    private fun attemptRegister(mEmailView: EditText, mPasswordView: EditText) {
        val (email, password) = validateUserAndPassword(mEmailView, mPasswordView)
        var name: String? = null
        mNameEditText.error = null

        if (mNameEditText.text.isNotEmpty()) {
            name = mNameEditText.text.toString()
        } else {
            mNameEditText.error = getString(R.string.error_no_name)
            mNameEditText.requestFocus()
        }

        if (email == null || password == null || name == null) return

        repository.performRegister(email, password).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                repository.performSignIn(email, password).addOnCompleteListener {
                    if (it.isSuccessful) {
                        repository.setUserName(name).addOnCompleteListener {
                            resultListener.onResult(Activity.RESULT_OK)
                        }
                    }
                }
            } else {
                var view: View? = null
                when (task.exception) {
                    is FirebaseAuthUserCollisionException -> {
                        attemptLogin(mEmailView, mPasswordView)
                        changeLayoutToRegister(false)
                    }
                    is FirebaseAuthEmailException -> {
                        view = mEmailView
                        mEmailView.error = getString(R.string.error_wrong_email_address)
                    }
                    else -> Toast.makeText(context, "Error registering the user, try later!",
                            Toast.LENGTH_LONG).show()
                }
                view?.requestFocus()
            }
        }
    }
}
