package com.proambiente.pineapplemarket;

import android.os.Bundle;
import android.util.Pair;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.proambiente.pineapplemarket.model.Product;
import com.proambiente.pineapplemarket.ui.dialogs.AddProductDialogFragment;
import com.proambiente.pineapplemarket.ui.main.ListsViewModel;
import com.proambiente.pineapplemarket.ui.products.Graph;
import com.proambiente.pineapplemarket.ui.products.PricesAdapter;
import com.proambiente.pineapplemarket.ui.products.ProductViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ProductActivity extends AppCompatActivity implements AddProductDialogFragment.OnResult {
    public static final String ARG_PRODUCT = "PRODUCT";
    private Product product;
    private List<com.proambiente.pineapplemarket.model.List> lists;
    private Graph graphFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_activity);

        ProductViewModel productViewModel = ViewModelProviders.of(this)
                .get(ProductViewModel.class);
        ListsViewModel listsViewModel = ViewModelProviders.of(this)
                .get(ListsViewModel.class);

        Toolbar toolbar = findViewById(R.id.product_toolbar);
        setSupportActionBar(toolbar);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Product");

        Bundle args = getIntent().getExtras();

        if (args != null) {
            product = (Product) args.getSerializable(ARG_PRODUCT);
        }

        RecyclerView recyclerView = findViewById(R.id.recycler_prices);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,
                RecyclerView.VERTICAL, false));

        ImageView productImage = findViewById(R.id.product_image);

        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.generic_product)
                .error(R.drawable.generic_product)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);

        Glide.with(this)
                .load(product.getImage())
                .apply(options)
                .into(productImage);

        ((TextView) findViewById(R.id.product_label)).setText(product.getName());

        PricesAdapter adapter = new PricesAdapter(new ArrayList<>(), getResources());
        recyclerView.setAdapter(adapter);

        TextView averagePrices = findViewById(R.id.average_price_label);
        graphFragment = new Graph();

        View graphView = findViewById(R.id.prices_graph);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.prices_graph, graphFragment)
                .commitNow();

        if (product.getPrices() != null && product.getPrices().size() > 0) {
            graphView.setVisibility(View.VISIBLE);
        } else {
            graphView.setVisibility(View.GONE);
        }

        productViewModel.getPricesByProduct(product).observe(this, pairs -> {
            if (pairs == null)
                return;

            if (pairs.size() > 0) {
                product.setPrices((ArrayList<Pair<String, Double>>) pairs.get(0));
            }

            if (product.getPrices() != null)
                adapter.setPrices(product.getPrices(), getResources());

            if (product.getPrices() != null && product.getPrices().size() > 0) {
                averagePrices.setText(String.format(Locale.getDefault(), "%s",
                        product.getPrices().get(0).second.toString().toUpperCase()));
                graphView.setVisibility(View.VISIBLE);
            } else {
                graphView.setVisibility(View.GONE);
            }
        });

        listsViewModel.getAllLists().observe(this, listsUser -> lists = listsUser);

        productViewModel.getHistoricalPricesByProduct(product).observe(this,
                pairs2 -> graphFragment.generateDataLine(pairs2));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void showNoticeDialog(View v) {
        String names[] = new String[lists.size()];
        String ids[] = new String[lists.size()];

        for (int i = 0; i < lists.size(); i++) {
            names[i] = lists.get(i).getName();
            ids[i] = lists.get(i).getId();
        }

        DialogFragment addProductDialog = AddProductDialogFragment.newInstance(
                names, ids, product);
        addProductDialog.show(getSupportFragmentManager(), "NoticeDialogFragment");
    }

    @Override
    public void productAddedInList(boolean added) {
        if (added) {
            Toast.makeText(this, "Producto añadido a lista",
                    Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Este producto ya esta en tu lista",
                    Toast.LENGTH_SHORT).show();
        }
    }
}
