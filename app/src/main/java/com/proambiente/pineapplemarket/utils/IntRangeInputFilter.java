package com.proambiente.pineapplemarket.utils;

import android.text.InputFilter;
import android.text.Spanned;

public class IntRangeInputFilter implements InputFilter {
    private int min;
    private int max;
    private StringBuilder sb;

    public IntRangeInputFilter(int min, int max) {
        this.min = min;
        this.max = max;
        sb = new StringBuilder();
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        if (source.equals(""))
            return null;

        sb.setLength(0);
        sb.append(dest).insert(dstart, source);

        int n;
        try {
            n = Integer.valueOf(sb.toString());
            if (n > max || n < min) {
                return "";
            }
        } catch (Exception e) {
            return "";
        }

        return null;
    }
}
