package com.proambiente.pineapplemarket.utils;

import android.util.Pair;

import com.google.firebase.database.DataSnapshot;
import com.proambiente.pineapplemarket.model.List;
import com.proambiente.pineapplemarket.model.ListProduct;
import com.proambiente.pineapplemarket.model.Market;
import com.proambiente.pineapplemarket.model.Product;
import com.proambiente.pineapplemarket.model.User;

import java.util.ArrayList;

import androidx.annotation.NonNull;

public class Factory {
    private Factory() {
    }

    public static ListProduct buildListProduct(@NonNull DataSnapshot ds) {
        ListProduct pr = ds.getValue(ListProduct.class);
        if (pr == null || ds.getKey() == null) return null;
        pr.setId(ds.getKey());
        return pr;
    }

    public static Product buildProduct(DataSnapshot ds) {
        Product pr = ds.getValue(Product.class);
        if (pr == null) return null;
        pr.setId(ds.getKey());
        return pr;
    }

    public static java.util.List<Pair<String, Double>> buildPrices(DataSnapshot ds) {
        java.util.List<Pair<String, Double>> prices = new ArrayList<>();
        for (DataSnapshot data : ds.getChildren()) {
            prices.add(new Pair<>(data.getKey(), Double.parseDouble(data.getValue().toString())));
        }

        return prices;
    }

    public static java.util.List<Pair<String, Double>> buildHistoricalPrices(DataSnapshot ds) {
        java.util.List<Pair<String, Double>> prices = new ArrayList<>();
        prices.add(new Pair<>(ds.getKey(), Double.parseDouble(ds.child("average").getValue().toString())));
        return prices;
    }

    public static List buildList(DataSnapshot ds) {
        List ls = ds.getValue(List.class);
        ls.setId(ds.getKey());
        return ls;
    }

    public static Market buildMarket(DataSnapshot ds) {
        Market ls = ds.getValue(Market.class);
        return ls;
    }

    public static User buildUser(DataSnapshot ds) {
        User ls = ds.getValue(User.class);
        return ls;
    }
}
