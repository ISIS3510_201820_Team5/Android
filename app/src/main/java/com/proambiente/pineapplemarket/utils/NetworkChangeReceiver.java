package com.proambiente.pineapplemarket.utils;

import androidx.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.FragmentActivity;

import com.proambiente.pineapplemarket.ui.home.ConnectivityViewModel;

public class NetworkChangeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Connectivity connectivity = Connectivity.getInstance();

        ConnectivityViewModel viewModel = ViewModelProviders
                .of((FragmentActivity) context).get(ConnectivityViewModel.class);

        viewModel.postConnected(connectivity.isOnline(context));
    }
}
