package com.proambiente.pineapplemarket.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import androidx.annotation.RequiresApi;

import java.util.ArrayList;

public class Connectivity {
    private static Connectivity connectivity;

    private Connectivity() {
    }

    public static Connectivity getInstance() {
        if (connectivity == null)
            connectivity = new Connectivity();
        return connectivity;
    }

    public boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ArrayList<Integer> connectionType(Context context) {
        ConnectivityManager connMgr =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        ArrayList<Integer> connectionTypes = new ArrayList<>();
        assert connMgr != null;
        for (Network network : connMgr.getAllNetworks()) {
            NetworkInfo networkInfo = connMgr.getNetworkInfo(network);
            if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                if (networkInfo.isConnected())
                    connectionTypes.add(ConnectivityManager.TYPE_WIFI);
            }
            if (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                if (networkInfo.isConnected())
                    connectionTypes.add(ConnectivityManager.TYPE_MOBILE);
            }
        }
        return connectionTypes;
    }
}
