package com.proambiente.pineapplemarket;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.proambiente.pineapplemarket.repository.DataRepository;
import com.proambiente.pineapplemarket.ui.home.ConnectivityViewModel;
import com.proambiente.pineapplemarket.ui.main.ExploreFragment;
import com.proambiente.pineapplemarket.ui.main.HomeFragment;
import com.proambiente.pineapplemarket.ui.main.ListsFragment;
import com.proambiente.pineapplemarket.utils.NetworkChangeReceiver;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

public class MainActivity extends AppCompatActivity {
    private HomeFragment homeFragment;
    private ListsFragment listsFragment;
    private ExploreFragment exploreFragment;
    private NetworkChangeReceiver receiver;
    private Snackbar snackbar;
    private View viewSnackBar;
    private ConnectivityViewModel viewModel;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (receiver != null) {
            this.unregisterReceiver(receiver);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        viewSnackBar = this.getWindow().getDecorView().findViewById(R.id.viewSnack);
        snackbar = Snackbar.make(
                viewSnackBar,
                getApplicationContext().getString(R.string.no_network_connection),
                Snackbar.LENGTH_INDEFINITE);

        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new NetworkChangeReceiver();
        this.registerReceiver(receiver, filter);

        viewModel = ViewModelProviders
                .of(this).get(ConnectivityViewModel.class);

        if (savedInstanceState == null) {
            homeFragment = new HomeFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, homeFragment)
                    .commitNow();
        }

        MaterialSearchBar materialSearchBar = findViewById(R.id.main_activity_search_bar);
        materialSearchBar.inflateMenu(R.menu.toolbar);

        materialSearchBar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count >= 500)
                    materialSearchBar.setText(s.subSequence(0, 499).toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        materialSearchBar.setOnClickListener((view) -> {
            if (exploreFragment != null && exploreFragment.isVisible())
                startActivity(new Intent(MainActivity.this,
                        SupermarketActivity.class));
            else {
                startActivity(new Intent(MainActivity.this,
                        SearchableActivity.class));
            }
        });

        materialSearchBar.setOnSearchActionListener(
                new MaterialSearchBar.OnSearchActionListener() {
                    @Override
                    public void onSearchStateChanged(boolean enabled) {
                        if (enabled)
                            startActivity(new Intent(MainActivity.this,
                                    SearchableActivity.class));
                    }

                    @Override
                    public void onSearchConfirmed(CharSequence text) {
                    }

                    @Override
                    public void onButtonClicked(int buttonCode) {
                    }
                });

        materialSearchBar.getMenu().setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_scan:
                    startActivity(new Intent(this, BarcodeActivity.class));
                    return true;
                case R.id.action_logout:
                    DataRepository.getInstance(getApplication()).performSignOut();
                    startActivity(new Intent(MainActivity.this, SplashActivity.class));
                    finish();
                    return true;
            }
            return false;
        });

        viewModel.getConnected().observe(this, connected -> {
            if (connected == null) return;

            showSnackbar(!connected);

            if (connected) {
                materialSearchBar.setHint("Search...");
                materialSearchBar.setSearchIconTint(getResources().getColor(R.color.transparentBlack));
                materialSearchBar.setClickable(true);
                materialSearchBar.setFocusable(true);
            } else {
                materialSearchBar.setText("Offline mode");
                materialSearchBar.setSearchIconTint(getResources().getColor(R.color.selector));
                materialSearchBar.disableSearch();
                materialSearchBar.setClickable(false);
                materialSearchBar.setFocusable(false);
            }
        });

        BottomNavigationView bottomNavigationView = findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            Fragment fragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    if (homeFragment == null)
                        homeFragment = new HomeFragment();
                    fragment = homeFragment;
                    break;
                case R.id.navigation_dashboard:
                    if (exploreFragment == null)
                        exploreFragment = new ExploreFragment();
                    fragment = exploreFragment;
                    break;
                case R.id.navigation_notifications:
                    if (listsFragment == null)
                        listsFragment = new ListsFragment();
                    fragment = listsFragment;
                    break;
            }
            return loadFragment(fragment);
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        Boolean connected = viewModel.getConnected().getValue();
        if (connected == null) return;

        showSnackbar(!connected);
    }

    private void showSnackbar(boolean show) {
        if (show) {
            snackbar.show();
            viewSnackBar.setVisibility(View.VISIBLE);
        } else {
            snackbar.dismiss();
            viewSnackBar.setVisibility(View.GONE);
        }
    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, fragment).commit();
            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == ExploreFragment.MY_PERMISSIONS_REQUEST_LOCATION) {
            exploreFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
